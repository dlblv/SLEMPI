ifneq '$(DEBUG)' ''
	DEBUGFLAG = -D DEBUG
endif

CXX       = mpicxx
CXXFLAGS  = -Wall -Werror -Wpedantic -std=c++14 -I. $(DEBUGFLAG)
NAME	  = slempi
N		  = 4
OBJS	  = io.o main.o func.o

all: $(NAME)

$(NAME): $(OBJS)
	@echo -e "\e[31m===Linking $(OBJS) together\e[39m"
	@$(CXX) $(OBJS) -o $(NAME)
	@echo -e "\e[32m===Ready! Run \e[36mmake run\e[32m to start!\e[39m"

$(OBJS): %.o: %.cpp
	@echo -e "\e[31m===Compiling $@ from $<\e[39m"
	$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo -e "\e[32m===Compiled $@\e[39m"

run: $(NAME)
	mpirun -n $(N) ./$(NAME)

clean:
	@echo -e "\e[32m===Cleaning\e[39m"
	@rm -rf *.o $(NAME)


.PHONY: clean run
