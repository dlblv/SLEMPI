#include <include/io.h>
#include <include/func.h>
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <algorithm>

/*Print error message*/
void alert(int errcode) {
	std::string errstr = "";
	switch (errcode){
		case -1:
			errstr = "Ошибка ввода с клавиатуры!";
		break;

		case -2:
			errstr = "Ошибка открытия файла!";
		break;

		case -3:
			errstr = "Ошибка чтения из файла!";
		break;

		case -4:
			errstr = "Ошибка выделения памяти!";
		break;
	}

	std::cerr << errstr << " Код ошибки: " << errcode << std::endl;
}

void printVector(const int N, const double *A){
	std::cout << "(";
	std::cout << std::fixed << std::setprecision(3);
	for (int i = 0; i < N; ++i)
		std::cout << A[i] << ", ";
	std::cout << "\b\b)" << std::endl;
}

void printMatrix(const int N, const double *A, const double *B){
	std::cout << std::fixed << std::setprecision(3);
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j)
			std::cout << A[i*N+j] << " ";

		std::cout << "| " << B[i] << std::endl;
	}
}

void printMatrixCut(const int N, const double *A, const double *B){
	std::cout << std::fixed << std::setprecision(3);
	for (int i = 0; i < std::min(N, 10); ++i) {
		for (int j = 0; j < std::min(N,10); ++j)
			std::cout << A[i*N+j] << " ";

		std::cout << "| " << B[i] << std::endl;
	}
}

int readMatrix(int &N, double *&A, double *&B){
	std::cout << "Вводим из файла или по формуле? (y/anykey):";
	char inputType;
	if (!(std::cin >> inputType)) return -1;
	switch (inputType) {
		case 'y': {
			std::string filename;
			std::cout << "Введите имя файла: ";
			if (!(std::cin >> filename)) return -1;
			std::ifstream input(filename);
			if (!(input.is_open())) return -2;

			if (!(input >> N))
			{
				input.close();
				return -3;
			}

			A = new double[N*N];
			B = new double[N];

			for (int i = 0; i < N; i++){
				for (int j = 0; j < N; j++)
					if (!(input >> A[i*N+j])){
						delete [] A;
						delete [] B;
						input.close();
						return -3;
					}

				if (!(input >> B[i])){
					delete [] A;
					delete [] B;
					input.close();
					return -3;
				}
			}

			input.close();
		}
		break;

		default: {
			std::cout << "Введите размерность матрицы: ";
			if (!(std::cin >> N)) return -1;

			A = new double[N*N];
			B = new double[N];
			if ((A == nullptr)||(B==nullptr)) return -4;

			for (int i = 0; i < N; i++){
				for (int j = 0; j < N; j++)
					A[i*N + j] = gilb(i,j);

				B[i] = 0;
				for (int j = 0; j < N; j +=2)
					B[i] += A[i*N + j];
			}
		}
		break;
	}
	return 0;
}
