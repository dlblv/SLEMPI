﻿/*
 * Message tags:
 * 1)N
 * 2)A-main
 * 3)B-main
 * 4)max
 * 5)pmax
 * 6)A-maxstr (exchange)
 * 7)B-maxstr (exchange)
 * 8)A-ist (exchange)
 * 9)B-ist (exchange)
 * 10)A-ist (broadcast)
 * 11)B-ist (broadcast)
 * 12)Solution
 * 13)mytime
 */

#include <include/func.h>
#include <include/io.h>
#include <algorithm>
#include <mpi.h>
#include <chrono>
#include <cmath>
#include <iomanip>

int main(int argc, char ** argv){
	int rank = 0, /*number of current process in COMM*/
		size = 1, /*count of all*/
		N    = 0, /*matrix size*/
		myN  = 0; /*count of personal strings*/

	MPI::Init(argc, argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	double *myA = nullptr,
		   *myB = nullptr,
		   *A   = nullptr, /*only for rank = 0*/
		   *B   = nullptr, /*only for rank = 0*/
		   *X   = nullptr, /*only for rank = 0*/
		   mytime = 0;


	if (!rank){
		std::cout << "Процессов запущено: " << size << std::endl;

		int readStatus = readMatrix(N,A,B);
		if (readStatus){
			alert(readStatus);
			MPI_Abort(MPI_COMM_WORLD, readStatus);
			return readStatus;
		}
#if defined(DEBUG)
		printMatrix(N, A, B);
#else
		printMatrixCut(N, A, B);
#endif
		MPI_Barrier(MPI_COMM_WORLD); /*barrier 1 (split)*/

		for (int i = 1; i < size; ++i)
			MPI_Send(&N, 1, MPI_INT, i, 1, MPI_COMM_WORLD);  /*send message 1*/

	} else{
		MPI_Barrier(MPI_COMM_WORLD); /*barrier 1 (split)*/
		MPI_Recv(&N, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 1*/
	}
	myN = N/size + ((rank < N%size)?1:0);
	myA = new double [myN*N];
	myB = new double [myN];
	MPI_Barrier(MPI_COMM_WORLD); /*barrier 2*/

	if (!rank) {
		for (int i = 0, k = 0; i < N; ++i)
			if (i%size) {
				MPI_Send(A+i*N, N, MPI_DOUBLE, i%size, 2, MPI_COMM_WORLD); /*send message 2*/
				MPI_Send(B+i, 1, MPI_DOUBLE, i%size, 3, MPI_COMM_WORLD); /*send message 3*/
			} else {
				for (int j = 0; j < N; ++j)
					myA[k*N+j] = A[i*N+j];

				myB[k++] = B[i];
			}

		MPI_Barrier(MPI_COMM_WORLD); /*barrier 3 (split)*/
	} else{
		for (int i = 0; i < myN; ++i){
			MPI_Recv(myA+i*N, N, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 2*/
			MPI_Recv(myB+i, 1, MPI_DOUBLE, 0, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 3*/
		}
		MPI_Barrier(MPI_COMM_WORLD); /*barrier 3 (split)*/
	}

	mytime = MPI_Wtime();
	std::chrono::high_resolution_clock::time_point t_start= std::chrono::high_resolution_clock::now();

	for (int i = 0; i < N; ++i){
		MPI_Barrier(MPI_COMM_WORLD); /*barrier 4 (after entrance to loop)*/
		int local_i = (i - rank)/size;

		/*will search for max if has needed rows*/
		double max = 0;
		int maxstr = 0;
		if ((myN-1)*size + rank >= i){
			max = std::abs(myA[(myN - 1)*N + i]);
			maxstr = myN - 1;
			for (int j = myN - 1; j*size + rank >= i; --j){
				if (max < fabs(myA[j*N + i])){
					max = fabs(myA[j*N + i]);
					maxstr = j;
				}
			}
			if (rank)
				MPI_Send(&max, 1, MPI_DOUBLE, 0, 4, MPI_COMM_WORLD); /*send message 4*/
#if defined(DEBUG)
			std::cout << i << " " << rank << ": found max=" << std::fixed << std::setprecision(3) << max << " maxstr=" << maxstr << std::endl;
#endif
		}

		/*main proc gets maxs and compares counting pmax, then sends it to children*/
		int pmax = -1;
		if (!rank){
			pmax = 0; /*pretend self to be pmax*/
			for (int j = 1; j < size; ++j)
				if ((myNforproc(N,j,size)-1)*size + j >= i){
					double tempmax = 0;
					MPI_Recv(&tempmax, 1, MPI_DOUBLE, j, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 4*/
#if defined(DEBUG)
					std::cout << i << " " << rank << ": got max=" << tempmax << " from proc " << j << std::endl;
#endif
					if (tempmax > max){
						max = tempmax;
						pmax = j;
					}
#if defined(DEBUG)
					std::cout << i << " " << rank << ": pmax=" << pmax << std::endl;
#endif
				}
			MPI_Barrier(MPI_COMM_WORLD); /*barrier 5 (split)*/
			for (int j = 1; j < size; ++j)
				MPI_Send(&pmax, 1, MPI_INT, j, 5, MPI_COMM_WORLD);/*send message 5*/
		} else{
			MPI_Barrier(MPI_COMM_WORLD); /*barrier 5 (split)*/
			MPI_Recv(&pmax, 1, MPI_INT, 0, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);/*receive message 5*/

#if defined(DEBUG)
			std::cout << rank << ": got pmax=" << pmax << std::endl;
#endif
		}

		MPI_Barrier(MPI_COMM_WORLD); /*barrier 6*/

		/*now everybody knows pmax and both pmax and i%size need to swap i-st and pmax.maxstr rows*/
		if ((rank == pmax)&&(rank == i%size)){
#if defined(DEBUG)
			std::cout << i << " " << rank << ": i'm both pmax and i_size" << std::endl;
#endif
			swapArr(N, myA + local_i*N, myA + maxstr*N);
			std::swap(myB[local_i], myB[maxstr]);
		} else if (rank == pmax){
#if defined(DEBUG)
			std::cout << i << " " << rank << ": i'm pmax" << std::endl;
#endif
			MPI_Send(myA + maxstr*N, N, MPI_DOUBLE, i%size, 6, MPI_COMM_WORLD); /*send message 6*/
			MPI_Send(myB + maxstr, 1, MPI_DOUBLE, i%size, 7, MPI_COMM_WORLD); /*send message 7*/
			MPI_Recv(myA + maxstr*N, N, MPI_DOUBLE, i%size, 8, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 8*/
			MPI_Recv(myB + maxstr, 1, MPI_DOUBLE, i%size, 9, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 9*/
		} else if (rank == i%size){
#if defined(DEBUG)
			std::cout << i << " " << rank << ": i'm i_size" << std::endl;
#endif
			MPI_Send(myA + local_i*N, N, MPI_DOUBLE, pmax, 8, MPI_COMM_WORLD); /*send message 8*/
			MPI_Send(myB + local_i, 1, MPI_DOUBLE, pmax, 9, MPI_COMM_WORLD); /*send message 9*/
			MPI_Recv(myA + local_i*N, N, MPI_DOUBLE, pmax, 6, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 6*/
			MPI_Recv(myB + local_i, 1, MPI_DOUBLE, pmax, 7, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 7*/
		}
		MPI_Barrier(MPI_COMM_WORLD); /*barrier 7*/

#if defined(DEBUG)
			if (!rank) std::cout << "i%size exchanged with pmax" << std::endl;
#endif

		if (rank == i%size){
			double divisor = myA[local_i*N + i];
			for (int j = i; j < N; ++j)
				myA[local_i*N+j] /= divisor;

			myB[local_i] /= divisor;

			for (int j = 0; j < size; ++j)
				if(j != rank){
					MPI_Send(myA + local_i*N, N, MPI_DOUBLE, j, 10, MPI_COMM_WORLD); /*send message 10*/
					MPI_Send(myB + local_i, 1, MPI_DOUBLE, j, 11, MPI_COMM_WORLD); /*send message 11*/
				}

			MPI_Barrier(MPI_COMM_WORLD); /*barrier 8 (split)*/

			for (int j = 0; j < myN; ++j)
				if (j != local_i){
					double multiplier = myA[j*N + i];
					for (int k = i; k < N; ++k)
						myA[j*N + k] -= multiplier*myA[local_i*N + k];

					myB[j] -= multiplier*myB[local_i];
				}
		} else {
			double *istrow = new double [N],
				   istb = 0;
			MPI_Recv(istrow, N, MPI_DOUBLE, i%size, 10, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 10*/
			MPI_Recv(&istb, 1, MPI_DOUBLE, i%size, 11, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 11*/

			MPI_Barrier(MPI_COMM_WORLD); /*barrier 8 (split)*/

			for (int j = 0; j < myN; ++j){
				double multiplier = myA[j*N + i];
				for (int k = i; k < N; ++k)
					myA[j*N + k] -= multiplier*istrow[k];

				myB[j] -= multiplier*istb;
			}
			delete [] istrow;
		}


		MPI_Barrier(MPI_COMM_WORLD); /*barrier 9 (before exit from loop)*/
	}

	mytime = MPI_Wtime() - mytime;
	std::chrono::high_resolution_clock::time_point t_finish = std::chrono::high_resolution_clock::now();

	if (!rank){
		X = new double [N];
		for (int i = 0, k = 0; i < N; ++i)
			if (i%size)
				MPI_Recv(X + i, 1, MPI_DOUBLE, i%size, 12, MPI_COMM_WORLD, MPI_STATUS_IGNORE); /*receive message 12*/
			else
				X[i] = myB[k++];
		std::cout << "Вектор решений: ";
		printVector(N, X);
		std::cout << "Норма невязки: " << std::scientific << nevNorm(N, A, B, X) << std::endl;
		std::cout << "Норма разности с каноническим решением гильбертовой матрицы: " << std::scientific << diffNorm(N, X) << std::endl;
	} else{
		for (int i = 0; i < myN; ++i)
			MPI_Send(myB + i, 1, MPI_DOUBLE, 0, 12, MPI_COMM_WORLD); /*send message 12*/
	}

	MPI_Barrier(MPI_COMM_WORLD); /*barrier 10*/

	std::cout << "Время процесса "<< rank << ": " << mytime << " сек." << std::endl;
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t_finish - t_start);
	std::cout << "Реальное время процесса " << rank << ": " << time_span.count() << " сек." << std::endl;
	/*clearing memory*/
	if (!rank){
		delete [] A;
		delete [] B;
		delete [] X;
	}
	delete [] myA;
	delete [] myB;

	MPI_Finalize();
	return 0;
}
