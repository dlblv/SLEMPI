#pragma once

/*Print error message*/
void alert(int errcode);

void printVector(const int N, const double *A);

void printMatrix(const int N, const double *A, const double *B);

void printMatrixCut(const int N, const double *A, const double *B);

int readMatrix(int &N, double *&A, double *&B);
