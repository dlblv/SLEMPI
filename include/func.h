#pragma once

void swapArr(const int N, double *A, double *B);

int myNforproc(const int N, const int rank, const int size);

double nevNorm(const int N, const double *A, const double *B, const double *X);

double diffNorm(const int N, const double *X);


/*Function returning Gilbert matrix element*/
double gilb(const int i, const int j);

/*Fill matrix finc template*/
double fill(const int i, const int j);
