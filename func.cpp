#include <include/func.h>
#include <cmath>
#include <algorithm>

void swapArr(const int N, double *A, double *B){
	for (int i = 0; i < N; ++i)
		std::swap(A[i],B[i]);
}

int myNforproc(const int N, const int rank, const int size){
	return N/size + ((rank < N%size)?1:0);
}

double nevNorm(const int N, const double *A, const double *B, const double *X){
	double result = 0;
	for (int i = 0; i < N; ++i){
		double t = 0;
		for (int j = 0; j < N; ++j)
			t += A[i*N+j]*X[j];

		t -= B[i];
		result += t*t;
	}
	return std::sqrt(result);
}

double diffNorm(const int N, const double *X){
	double result = 0;
	for (int i = 0; i < N; ++i)
		result += pow(X[i] - (i + 1)%2, 2);

	return std::sqrt(result);
}


/*Function returning Gilbert matrix element*/
double gilb(const int i, const int j){
	return 1.0/(i+j+1);
}

/*Fill matrix finc template*/
double fill(const int i, const int j){
	return 1.0/(i+j+1);
}
